function Bus () {
    this.Model;
    this.Plate;
    this.Brand;
    this.Capacity;
}
Bus.prototype.getOptionHTML = function() {
    return "<option value=" + this.Plate + ">" + this.Model + "</option>";
};
Bus.prototype.getTrHTML = function() {
    var result = "<tr>"
        + "<td>" + this.Model + "</td>"
        + "<td>" + this.Plate + "</td>"
        + "<td>" + this.Brand + "</td>"
        + "<td>" + this.Capacity + "</td>"
        + "</tr>";

    return result;
};

function Driver () {
    this.Id;
    this.Name;
    this.RegisterDate = new Date();
}
Driver.prototype.getOptionHTML = function() {
    return "<option value=" + this.Id + ">" + this.Name + "</option>";  
};
Driver.prototype.getTrHTML = function() {
    var result = "<tr>"
        + "<td>" + this.Id + "</td>"
        + "<td>" + this.RegisterDate + "</td>"
        + "<td>" + this.Name + "</td>"
        + "</tr>";

    return result;  
};

function Route () {
    this.Id;
    this.Origin;
    this.Destination;
    this.otherA;
    this.otherB;
}
Route.prototype.getOptionHTML = function() {
    return "<option value=" + this.Id + ">" + this.Origin + "-" + this.Destination + "</option>";
};
Route.prototype.getTrHTML = function() {
    var result = "<tr>"
        + "<td>" + this.Id + "</td>"
        + "<td>" + this.Origin + "</td>"
        + "<td>" + this.Destination + "</td>"
        + "<td>" + this.otherA + "</td>"
        + "<td>" + this.otherB + "</td>"
        + "</tr>";

    return result;
};

function Dispatch () {
    this.Bus;
    this.Driver;
    this.Route;
}
Dispatch.prototype.getTrHTML = function () {
    var result = "<tr>"
        + "<td>" + this.Bus.Plate + "</td>"
        + "<td>" + this.Driver.Name + "</td>"
        + "<td>" + this.Route.Origin + "-" + this.Route.Destination + "</td>"
        + "</tr>";

    return result;
};